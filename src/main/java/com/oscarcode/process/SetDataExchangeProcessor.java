package com.oscarcode.process;

import com.oscarcode.Apachecamel.Order;
import org.apache.camel.Exchange;
import org.apache.camel.Processor;
import org.springframework.stereotype.Component;

import java.util.Optional;


public class SetDataExchangeProcessor implements Processor {

    @Override
    public void process(Exchange exchange) throws Exception {

        Order order = exchange.getIn().getBody(Order.class);

        Order orderHeader = exchange.getIn().getHeader("Type", Order.class);
        System.out.println(orderHeader);
        System.out.println(order.getName());

    }
}
