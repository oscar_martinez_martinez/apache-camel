package com.oscarcode.Apachecamel;

import com.oscarcode.Apachecamel.Models.User;
import org.apache.camel.Exchange;

import java.util.ArrayList;
import java.util.List;

public class processRequestGetUsers implements org.apache.camel.Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        List<User> userList = new ArrayList<>();

        Integer userId = exchange.getIn().getHeader("userId", Integer.class);

        if(userId != null) {

            User user = new User("Oscar", 22);
            User user2 = new User("Raul", 22);

            userList.add(user);
            userList.add(user2);

            int i = 0;
            while (i < userId) {
                userList.add(user);
                i++;
            }

            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 200);
        }else{
            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, 404);
        }

        exchange.getOut().setBody(userList);

    }
}
