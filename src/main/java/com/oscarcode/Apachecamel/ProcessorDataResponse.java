package com.oscarcode.Apachecamel;

import com.oscarcode.Apachecamel.Models.Dog;
import org.apache.camel.Exchange;

public class ProcessorDataResponse implements org.apache.camel.Processor {
    @Override
    public void process(Exchange exchange) throws Exception {
        Dog dog = exchange.getIn().getBody(Dog.class);
        System.out.println("Body Response --> " + dog.getMessage());
    }
}
