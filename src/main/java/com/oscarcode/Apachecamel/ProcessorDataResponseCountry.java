package com.oscarcode.Apachecamel;

import com.oscarcode.Apachecamel.Models.Country;
import org.apache.camel.Exchange;

public class ProcessorDataResponseCountry implements org.apache.camel.Processor {
    @Override
    public void process(Exchange exchange) throws Exception {

       Country[] country  = exchange.getIn().getBody(Country[].class);
        for (Country count: country) {
            System.out.println(count.getName());
        }

        exchange.getOut().setBody(country);

    }
}
