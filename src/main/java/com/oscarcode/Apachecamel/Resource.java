package com.oscarcode.Apachecamel;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.oscarcode.Apachecamel.Models.Country;
import com.oscarcode.Apachecamel.Models.Dog;
import com.oscarcode.Apachecamel.Models.Payment;
import com.oscarcode.Apachecamel.Models.User;
import com.oscarcode.process.SetDataExchangeProcessor;
import org.apache.camel.BeanInject;
import org.apache.camel.Exchange;
import org.apache.camel.Header;
import org.apache.camel.Processor;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.component.jackson.JacksonDataFormat;
import org.apache.camel.component.kafka.KafkaConstants;
import org.apache.camel.model.dataformat.JacksonXMLDataFormat;
import org.apache.camel.model.dataformat.JsonLibrary;
import org.apache.camel.model.rest.RestBindingMode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;

import javax.print.attribute.standard.Media;
import javax.swing.*;

@Component
public class Resource extends RouteBuilder {

    JacksonDataFormat jacksonDataFormat;

    JacksonDataFormat jacksonDataFormatCountry;
    JacksonDataFormat jsonUser;

    JacksonDataFormat jacksonDataFormatPayment;


    int number = 1;

    public Resource(){
        this.jacksonDataFormat = new JacksonDataFormat(Payment.class);
        jacksonDataFormatCountry = new JacksonDataFormat(Country[].class);
        jsonUser = new JacksonDataFormat(User.class);
        jacksonDataFormatPayment = new JacksonDataFormat(Payment.class);
    }

    @Autowired
    private Service service;

    @BeanInject
    private OrderProccesor orderProccesor;


    @Override
    public void configure() throws Exception {



        /*

        //Trigger runs each second
        from("timer:simple?period=1000").log("Processed message")
                //.setBody(constant("Message to processed"))
                //.setHeader("Type", constant("Value header"))
                //.process(exchange -> {
                    //  System.out.println("1. Body in = " + exchange.getIn().getBody() );
                    //  Order order = new Order(10, "Pc", 90000.90);
                    //                      exchange.getOut().setBody(order);
//
                    //                      Order orderHeader = new Order(12, "Rm", 8000);
//
                    //         exchange.getOut().setHeader("Type", orderHeader);
               // })
                .process(exchange -> {
                    //number++;
                    //exchange.getOut().setHeader("id",  number);

                })
                .to("direct:httpRequestCountry")
                .end();



        from("direct:httpRequest")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .recipientList(simple("https://dog.ceo/api/breeds/image/random"))
                .unmarshal(jacksonDataFormat)
                .process(new ProcessorDataResponse())
                .end();

        from("direct:httpRequestCountry")
                .setHeader(Exchange.HTTP_METHOD, constant("GET"))
                .recipientList(simple("https://restcountries.eu/rest/v2/all?fields=name"))
                .unmarshal(jacksonDataFormatCountry)
                .process(new ProcessorDataResponseCountry())
                .choice() // Allows open sentence of the validation
                    .when(body().isNull())
                        .log("Body is empty")

                    .otherwise()
                        .split(body())
                            .process(exchange ->{
                                Country country = exchange.getIn().getBody(Country.class);
                                System.out.println("Split processed: " + country.getName());
                                exchange.getOut().setBody(country.getName());
                                exchange.getOut().setHeader("Value", "H");
                            })
                            .to("direct:processedMessage")
                .endChoice()
                .end();

        from("direct:processedMessage")
                .log("Start message")
                .log("Body = ${body}")
                //.process(new SetDataExchangeProcessor() )
                .end();





        restConfiguration()
                .component("servlet")
                .port(9090)
                .host("localhost")
                .corsHeaderProperty("Access-Control-Allow-Origin", "*")
                .corsHeaderProperty("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE")
                .corsHeaderProperty("'Access-Control-Allow-Headers", "Content-Type")
                .bindingMode(RestBindingMode.json);


        rest("/api/v1").get("/users").produces(MediaType.APPLICATION_JSON_VALUE).route().to("direct:processGetUser");
        rest("/api/v1").get("/users/{userId}").produces(MediaType.APPLICATION_JSON_VALUE).route().to("direct:processGetUser");

        from("direct:processGetUser")
                .process(new processRequestGetUsers())
                //.setBody(constant("Hola mundo"))
                .endRest();

        rest().get("/hello-world")
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route()
                .setBody(constant("Welcome to java techie"))
                .endRest();

        rest("/api/v1").post("/user").consumes(MediaType.APPLICATION_JSON_VALUE).type(User.class).outType(User.class)
                .route()
                .process(exchange ->{
                    User user = exchange.getIn().getBody(User.class);
                    exchange.getOut().setBody(user);
                }).endRest();


        rest().get("/getOrders")
                .produces(MediaType.APPLICATION_JSON_VALUE)
                .route()
                .setBody(()-> service.getOrders() )
                .endRest();

        rest().post("/addOrder").consumes(MediaType.APPLICATION_JSON_VALUE)
                .type(Order.class)
                .outType(Order.class)
                .route()
                .process(orderProccesor)
                .endRest();

        */

        from("kafka:Payment?brokers=localhost:9092")
                .log("    on the topic ${headers[kafka.TOPIC]}")
                .log("    on the partition ${headers[kafka.PARTITION]}")
                .log("    with the offset ${headers[kafka.OFFSET]}")
                .log("    with the key ${headers[kafka.KEY]}")
                .setHeader("Content-Type",simple("application/json"))
                .log("Message received from Kafka : ${body}")
                .process(exchange ->{
                    String payment = exchange.getIn().getBody(String.class);
                    System.out.println(payment.toString());
                    ObjectMapper mapper = new ObjectMapper();
                    Payment payment1 = mapper.readValue(payment.toString(), Payment.class);
                    System.out.println(payment1.getTotal());
                    exchange.getOut().setBody(payment1);
                }).marshal(jacksonDataFormat).to("direct:start");

        from("direct:start")
                .log("This is the consumer body: ${body} ")      // Message to send
                .setHeader(KafkaConstants.KEY, constant("Camel"))// Key of the message
                .to("kafka:PaymentToConsumer?brokers=localhost:9092");

    }
}
