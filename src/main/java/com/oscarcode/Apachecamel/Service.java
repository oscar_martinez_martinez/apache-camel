package com.oscarcode.Apachecamel;

import javax.annotation.PostConstruct;
import java.util.ArrayList;
import java.util.List;

@org.springframework.stereotype.Service
public class Service {

    private List<Order> list = new ArrayList<>();

    @PostConstruct
    public void initDB(){
        list.add(new Order(67, "Mobile", 5000));
        list.add(new Order(67, "Book", 5000));
    }


    public Order addOrder(Order order){
        list.add(order);
        return order;
    }

    public List<Order> getOrders(){
        return this.list;
    }

}
